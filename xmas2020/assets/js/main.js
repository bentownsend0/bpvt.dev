const snowContainer = document.querySelector('.snow-container');
const toText = document.querySelector('.to');
const messageText = document.querySelector('.message-text');

const setTo = nameValue => {
    const nameArray = nameValue.split(',').filter(name => name);
    const finalName = nameArray.pop();
    const to = nameArray.length < 1 ? `${finalName}` : `${nameArray.join(', ')} & ${finalName}`;

    toText.textContent = nameValue ? `Dear ${to},` : '';
}

const setMessage = message => messageText.textContent = message ? `${message},` : 'Lots of Love,';

const getParam = param => {
    const paramValue = new URLSearchParams(window.location.search).get(param);
    
    return paramValue;
}

const init = () => {
    const nameValue = getParam('name');
    const message = getParam('message');

    window.addEventListener('load', () => {
        const snowTotal = 100;
        for (let index = 0; index < snowTotal; index++) {
            const snow = document.createElement("div");
            snow.className = 'snow'
            snowContainer.append(snow)
        }
    })

    setTo(nameValue);
    setMessage(message);

    history.replaceState({}, null, `/xmas2020/?name=${nameValue}`);
}

init();