const snowContainer = document.querySelector('.snow-container');
const toText = document.querySelector('.to');
const messageText = document.querySelector('.message-text');
const wreathDrawn = document.querySelector('.wreath-drawn');
const reset = document.querySelector('.reset-button');

const setTo = nameValue => {
    const nameArray = nameValue.split(',').filter(name => name);
    const finalName = nameArray.pop();
    const to = nameArray.length < 1 ? `${finalName}` : `${nameArray.join(', ')} & ${finalName}`;

    toText.textContent = nameValue ? `Dear ${to},` : '';
}

const setMessage = message => messageText.textContent = message ? `${message},` : 'Lots of Love,';

const getParam = param => {
    const paramValue = new URLSearchParams(window.location.search).get(param);

    return paramValue;
}

const setPathLength = () => {
    const paths = wreathDrawn.querySelectorAll('path');
    for (let index = 0; index < paths.length; index++) {
        const length = paths[index].getTotalLength();
        paths[index].style.strokeDasharray = `${length}px`;
        paths[index].style.strokeDashoffset = `${length}px`;

        if (!paths[index].style.stroke) {
            paths[index].style.stroke = paths[index].style.fill;
            paths[index].style.fill = "none";
        }
    }
}

const init = () => {
    const nameValue = getParam('name');
    const message = getParam('message');

    window.addEventListener('load', () => {
        const snowTotal = 100;
        for (let index = 0; index < snowTotal; index++) {
            const snow = document.createElement("div");
            snow.className = 'snow';
            snowContainer.append(snow);
        }

        setPathLength();
        wreathDrawn.classList.add("unhide", "animate");
        document.querySelector('.wreath').classList.add("animate");
        document.querySelector('.greeting').classList.add("animate");
    });

    setTo(nameValue);
    setMessage(message);

    history.replaceState({}, null, `/xmas2021/?name=${nameValue}`);

    reset.addEventListener('click', () => {
        let currentWreath = document.querySelector('.wreath');
        let currentWreathDrawn = document.querySelector('.wreath-drawn');
        let currentGreeting = document.querySelector('.greeting');
        let newWreath = currentWreath.cloneNode(true);
        let newWreathDrawn = currentWreathDrawn.cloneNode(true);
        let newGreeting = currentGreeting.cloneNode(true);

        currentWreath.parentNode.replaceChild(newWreath, currentWreath);
        currentWreathDrawn.parentNode.replaceChild(newWreathDrawn, currentWreathDrawn);
        currentGreeting.parentNode.replaceChild(newGreeting, currentGreeting);
    });
}

init();