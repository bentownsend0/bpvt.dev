const body = document.body;
const snowContainer = document.querySelector('.snow-container');
const plane = document.querySelector('#plane');
const sleigh = document.querySelector('#sleigh');
const toText = document.querySelector('.to');
const messageText = document.querySelector('.message-text');

const getParam = (param) => {
    const paramValue = new URLSearchParams(window.location.search).get(param) || undefined;

    return paramValue;
}

const setTo = (nameValue) => {
    const nameArray = nameValue?.split(',').filter(name => name) || [];
    const finalName = nameArray.pop();
    const to = nameArray.length < 1 ? `${finalName}` : `${nameArray.join(', ')} & ${finalName}`;

    toText.textContent = nameValue ? `Dear ${to},` : '';
}

const init = () => {

    // get the name and message from the query string
    // if not available, use local storage
    // if not available, use default values
    // update the text content of the to and message elements
    const nameValue = getParam('name') || JSON.parse(localStorage.getItem('xmas2024'))?.name || '';
    const message = getParam('message') || JSON.parse(localStorage.getItem('xmas2024'))?.message || 'Lots of love';
    setTo(nameValue);
    messageText.textContent = message;

    window.addEventListener('load', () => {
        const snowTotal = 100;
        const initialValues = { y1: 10, y2: 90, delay: 0, speed: 5000 };
        const items = { "plane": { ...initialValues }, "sleigh": { ...initialValues } };

        body.style.setProperty(`--snow-total`, `${snowTotal}`);
        for (let index = 0; index < snowTotal; index++) {
            const snow = document.createElement("div");
            snow.className = 'snow'
            snowContainer.append(snow)
        }

        const runJourney = (item) => {
            //update itemY1 and itemY2
            items[item.id].y1 = Math.floor(Math.random() * 86) + 5;
            items[item.id].y2 = Math.floor(Math.random() * 86) + 5;
            body.style.setProperty(`--${item.id}-y1`, `${items[item.id].y1}%`);
            body.style.setProperty(`--${item.id}-y2`, `${items[item.id].y2}%`);

            // update item delay and speed
            items[item.id].delay = Math.floor(Math.random() * 5000) + 5000;
            items[item.id].speed = Math.floor(Math.random() * 5000) + 3000;
            body.style.setProperty(`--${item.id}-speed`, `${Math.floor(items[item.id].speed / 1000)}s`);

            // add animate class then remove it after plane has reached the end
            item.classList.add("animate");
            setTimeout(() => {
                item.classList.remove("animate");
            }, items[item.id].speed);
        }

        // Start the plane journey after delay
        // repeat the plane journey after planeDelay + planeSpeed
        runJourney(plane);
        runJourney(sleigh);
        setInterval(runJourney, items["plane"].delay + items["plane"].speed, plane);
        setInterval(runJourney, items["sleigh"].delay + items["sleigh"].speed, sleigh);
    })

    // Save the name and message to local storage then update the url
    localStorage.setItem('xmas2024', JSON.stringify({ name: nameValue, message: message }));

    history.replaceState({}, null, `/xmas2024/`);
}

init();